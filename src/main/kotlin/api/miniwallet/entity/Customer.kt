package api.miniwallet.entity

import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@Table(name = "customer")
@DynamicInsert
@DynamicUpdate
data class Customer(

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	var id: Long? = null,

	@Column(name = "customer_xid")
	var customerXid: String? = null,

	@Column(name = "token")
	var token: String? = null
)
