package api.miniwallet.controller

import api.miniwallet.model.InitRequest
import api.miniwallet.model.InitResponse
import api.miniwallet.service.MiniWalletService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping(value = ["/api/v1"])
class MiniWalletController @Autowired constructor(
	private val miniWalletService: MiniWalletService
) {
	@PostMapping("init")
	fun initialize(
		@Valid @ModelAttribute request: InitRequest
	): InitResponse {
		return miniWalletService.initialize(request)
	}
}