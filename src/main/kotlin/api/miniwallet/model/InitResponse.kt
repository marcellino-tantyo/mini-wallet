package api.miniwallet.model

import com.fasterxml.jackson.annotation.JsonProperty

data class InitResponse(

    @JsonProperty(value = "token")
    var token: String? = null
)
