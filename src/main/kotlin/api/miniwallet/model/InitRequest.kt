package api.miniwallet.model

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotEmpty

data class InitRequest (

    @field:NotEmpty
    @JsonProperty(value = "customer_xid")
    var customer_xid: String? = null
)