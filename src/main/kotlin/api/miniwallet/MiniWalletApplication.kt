package api.miniwallet

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MiniWalletApplication

fun main(args: Array<String>) {
	runApplication<MiniWalletApplication>(*args)
}
