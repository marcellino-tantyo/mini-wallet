package api.miniwallet.service

import api.miniwallet.model.InitRequest
import api.miniwallet.model.InitResponse
import api.miniwallet.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MiniWalletServiceImpl @Autowired constructor(
	private val customerRepository: CustomerRepository
) : MiniWalletService {

	override fun initialize(request: InitRequest): InitResponse {
		val customer = customerRepository.findByCustomerXid(request.customer_xid ?: "")

		when (customer) {
			null -> return InitResponse("Missing data token")
		}

		return InitResponse(customer?.token)
	}
}