package api.miniwallet.service

import api.miniwallet.model.InitRequest
import api.miniwallet.model.InitResponse

interface MiniWalletService {

    fun initialize(request: InitRequest): InitResponse
}